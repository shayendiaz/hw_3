class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.datetime :created_at
      t.boolean :private

      t.timestamps null: false
    end
  end
end
